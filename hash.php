<?php
	
	function SecureToken($text, $expire = '20') {
		$array['token']['current'] = substr(strtoupper(sha1(hash_hmac('md5', pack('N*', 0) . pack('N*', floor(time()/$expire)), $text, true))), 0, 20);
		$array['token']['next'] = substr(strtoupper(sha1(hash_hmac('md5', pack('N*', 0) . pack('N*', floor((time()+$expire)/$expire)), $text, true))), 0, 20);
		$array['expire'] = $expire-(time()%$expire);
		return json_encode($array);
	}

	echo SecureToken("76561198026886659");

?>