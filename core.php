<pre><?php
	
	define("UC_CORE", 1);
	include "./inc/init.php";

	$htaccess_file = file_get_contents(".htaccess");
	$htaccess_explode = explode(PHP_EOL, $htaccess_file);

	foreach ($htaccess_explode as $value) {
		$htaccess_separate = explode(" ", $value, 2);
		if($value != NULL) {
			$htaccess[$htaccess_separate['0']] = $htaccess_separate['1'];
		}
	}

	$path = str_replace($htaccess['RewriteBase'], NULL, $_SERVER['REDIRECT_URL']);
	if(!$path) { $path = 'index'; }
	list($action, $operation) = explode("/", $path, 2);
	if(!$operation) { $operation = 'index'; }
	echo $operation;