<?php

	//such line, much code, very wow.
	(!defined("UC_CORE")) ? die("You shall not pass!") : "";

	//We have some lang releated arrays
	$l = array();

	//First of all, we need to include english
	include UC_ROOT_DIR."inc/lang/lang.en.php";

	//The most shitty lang autodetection ever!
	if(getUserSetting("language") != NULL) {
		$langstring = getUserSetting("language");

		if(file_exists(UC_ROOT_DIR."inc/lang/lang.".$langstring.".php")) {
			include UC_ROOT_DIR."inc/lang/lang.".$langstring.".php";
		} else {
			include UC_ROOT_DIR."inc/lang/lang.en.php";
		}
	} elseif(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		$x = explode(",", $_SERVER['HTTP_ACCEPT_LANGUAGE']);
		$x = explode("-", $x['0']);

		$langstring = $x['0'];
		if(file_exists(UC_ROOT_DIR."inc/lang/lang.".$langstring.".php")) {
			include UC_ROOT_DIR."inc/lang/lang.".$langstring.".php";
		} else {
			include UC_ROOT_DIR."inc/lang/lang.en.php";
		}
	} else {
		include UC_ROOT_DIR."inc/lang/lang.en.php";
	}

	class LANG {
		public function sprintf($string) {
			$arg_list = func_get_args();
			$num_args = count($arg_list);

			for($i = 1; $i < $num_args; $i++)
			{
				$string = str_replace('{'.$i.'}', $arg_list[$i], $string);
			}

			return $string;
		}
	}