<?php

	class ParseDownQuotes extends ParsedownExtra {
		function __construct() {
			if(parent::version < '0.7.0') {
				throw new Exception('ParsedownExtra requires a later version of Parsedown');
			}

			$this->InlineTypes['{'] []= 'ColoredText';
			$this->inlineMarkerList .= '{';

			$this->InlineTypes['@'] []= 'QuoteText';
			$this->inlineMarkerList .= '@';
		}

		protected function inlineColoredText($Excerpt) {
			if (preg_match('/^{c:([#\w]\w+)}(.*?){\/c}/', $Excerpt['text'], $matches)) {
				return array(
					'extent' => strlen($matches[0]),
					'element' => array(
						'name' => 'span',
						'text' => $matches[2],
						'attributes' => array(
							'style' => 'color: '.$matches[1],
						),
					),
				);
			}
		}

		protected function inlineUrl($Excerpt) {
			if ($this->urlsLinked !== true or ! isset($Excerpt['text'][2]) or $Excerpt['text'][2] !== '/') {
				return;
			}

			if (preg_match('/\bhttps?:[\/]{2}[^\s<]+\b\/*/ui', $Excerpt['context'], $matches, PREG_OFFSET_CAPTURE)) {
				$Inline = array(
					'extent' => strlen($matches[0][0]),
					'position' => $matches[0][1],
					'element' => array(
						'name' => 'a',
						'text' => $matches[0][0],
						'attributes' => array(
							'href' => "https://ultimateclan.eu/linkfilter/".base64_encode($matches[0][0]),
						),
					),
				);

				return $Inline;
			}
		}

		protected function inlineQuoteText($Excerpt) {
			if (preg_match('/\B@(?<username>...+)#(?<id>\d+)/i', $Excerpt['text'], $matches)) {
				return array(
					'extent' => strlen($matches[0]),
					'element' => array(
						'name' => 'span',
						'text' => '@'.$matches['username'],
						'attributes' => array(
							'class' => 'tagging',
							'data-postid' => (int)$matches['id'],
						)
					),
				);
			}
		}
	}

?>