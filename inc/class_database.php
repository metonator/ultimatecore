<?php

	//Osobiscie wole napisac funckje na baze danych niż sie potem pierdolic w kodzie.

	class MyDatabaseConnector {

		public $databaselinker;
		public $query_count = 0;
		public $querylist = array();

		public function connect($connection_array) {
			if(!array_key_exists('hostname', $connection_array) || !array_key_exists('username', $connection_array) || !array_key_exists('password', $connection_array)) {
				throw new Exception("[ERROR] No database settings.");
			}

			$this->databaselinker = mysqli_connect($connection_array['hostname'], $connection_array['username'], $connection_array['password']) or die("[ERROR] Wrong username or password.");
			return $this->databaselinker;
		}

		public function select_db($databasename) {
			if(!$databasename) {
				throw new Exception("[ERROR] No database connection.");
			}

			mysqli_set_charset($this->databaselinker, 'utf8mb4');
			$slave_success = mysqli_select_db($this->databaselinker, $databasename);

			if(!$slave_success) {
				throw new Exception("[ERROR] Wrong database name.");
			}
			
			return $slave_success;
		}

		public function insert_id() {
			$id = mysqli_insert_id($this->databaselinker);
			return $id;
		}

		public function insert_query($table, $array) {
			if(!is_array($array)) {
				return false;
			}

			foreach($array as $field => $value) {
				$array[$field] = "'".$this->escape_string($value)."'";
			}

			$fields = "`".implode("`,`", array_keys($array))."`";
			$values = implode(",", $array);
			$this->query("INSERT INTO {$table} (".$fields.") VALUES (".$values.")");
			return $this->insert_id();
		}

		public function query($query) {
			$q = mysqli_query($this->databaselinker, $query);

			if (!$q) {
				throw new Exception('[ERROR] Looks like there\'s an error inside your query: '.mysqli_error($this->databaselinker).'.');
			}

			$this->query_count++;
			$this->querylist[$this->query_count] = $query;
			return $q;
		}

		public function escape_string($string) {
			return mysqli_real_escape_string($this->databaselinker, $string);
		}

		public function num_rows($query) {
			return mysqli_num_rows($query);
		}

		public function fetch_array($query) {
			$array = mysqli_fetch_array($query, MYSQLI_ASSOC);
			return $array;
		}

		public function get_list_of_queries() {
			return $this->querylist;
		}
	}

?>