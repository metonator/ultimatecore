<?php

function build_forumbits($pid=0, $depth=1, $alternative_view=0) {
	global $db, $fcache, $subforums, $l;

	if(empty($fcache[$pid]) || !is_array($fcache[$pid])) {
		return;
	}

	$parent_counters['threads'] = 0;
	$parent_counters['posts'] = 0;

	foreach($fcache[$pid] as $parent) {
		foreach($parent as $forum) {
			if(isset($fcache[$forum['fid']])) {
				$forum_info = build_forumbits($forum['fid'], $depth+1);

				$forum['threads'] += $forum_info['counters']['threads'];
				$forum['posts'] += $forum_info['counters']['posts'];

				$sub_forums = $forum_info['forum_list'];
			}

			if($depth > '2') {
				continue;
			}

			$forum['name'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['name']);
			$forum['description'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['description']);
			$forum['name'] = preg_replace("#&([^\#])(?![a-z1-4]{1,10};)#i", "&#038;$1", $forum['name']);
			$forum['description'] = preg_replace("#&([^\#])(?![a-z1-4]{1,10};)#i", "&#038;$1", $forum['description']);

			if($alternative_view == 0) {
				if($depth == 2) {
					eval("\$subforums .= \"".render_template("forumlist_subforum")."\";");
				}

				if($depth == 1) {
					$subs = $subforums;
					$subforums = "";
					eval("\$forum_list .= \"".render_template("forumlist_main_depth$depth")."\";");
				}
			} else {
				if(isset($forum)) {
					$replied_status = $l['forum_sub_nothreads'];
					eval("\$forum_list .= \"".render_template("forum_subforum_row")."\";");
				}
			}
		}
	}

	return array(
		"forum_list" => $forum_list,
		"counters" => $parent_counters,
	);
}

?>