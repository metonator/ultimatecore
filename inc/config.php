<?php
	
	//such line, much code, very wow.
	(!defined("UC_CORE")) ? die("You shall not pass!") : "";
	
	//Settings and Config
	$settings = array();

	//Cookie Releated
	$settings['cookie']['domain']		 = "";
	$settings['cookie']['path']			 = "/";

	//Core Releated
	$settings['core']['sitename']		 = "UltimateClan.gg";
	$settings['core']['siteurl']		 = "http://localhost/UltimateCore/";
	$settings['core']['closedinfo']		 = "";
	$settings['core']['default_avatar']	 = "";

	//MySQL Releated
	$settings['sql']['hostname']		 = "localhost";
	$settings['sql']['username']		 = "root";
	$settings['sql']['password']		 = NULL;
	$settings['sql']['port']			 = "3306";
	$settings['sql']['charset']			 = "utf8mb4";
	$settings['sql']['dbname']			 = "uc_core";

	//Steam Releated
	$settings['steam']['apikey']		 = "FBFD0A4EBA628D6B464A61D8BCD0E24E";
	$settings['steam']['hostname']		 = "localhost";
	$settings['steam']['identity']		 = "http://steamcommunity.com/openid";

	//Here are going some user settings defaults
	$userDefaults = array(
		'customQuote' 					=> '`this` user has not yet set his own Quote',
		'custom_nick' 					=> '0',
		'custom_avatar' 				=> '0',
		'country' 						=> 'XX',
		'snooper_nickname' 				=> '',
		'wormnet_nickname' 				=> '',
		'city' 							=> '',
		'birthdate'						=> '',
		'language'						=> '',
		'is_admin'						=> '0',
		'groups'						=> '1'
	);