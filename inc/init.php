<?php

	//such line, much code, very wow.
	(!defined("UC_CORE")) ? die("You shall not pass!") : "";

	// Report simple running errors
	error_reporting(E_ERROR | E_WARNING | E_PARSE);

	//Let's start whole session thing.
	session_start();

	//Some Autodetection
	if(!defined('UC_ROOT_DIR')) {
		define('UC_ROOT_DIR', dirname(dirname(__FILE__))."/");
	}

	//Okay, now let's include all libraries we have.
	include UC_ROOT_DIR."inc/config.php";
	include UC_ROOT_DIR."inc/vendor/LightOpenID/openid.php";
	include UC_ROOT_DIR."inc/vendor/WhichBrowser/vendor/autoload.php"; 			//Composer... i hate it... whoever created Composer should die immediately...
	include UC_ROOT_DIR."inc/vendor/Whoops/vendor/autoload.php"; 					//Composer... once FUCKIN again...
	include UC_ROOT_DIR."inc/vendor/ParseDown/Parsedown.php";
	include UC_ROOT_DIR."inc/vendor/ParseDown/ParsedownExtra.php";
	include UC_ROOT_DIR."inc/vendor/ParseDown/ParsedownQuotes.php";
	include UC_ROOT_DIR."inc/vendor/CountryHelper/CountryHelper.php";
	include UC_ROOT_DIR."inc/vendor/Turbo/Turbo.php";
	include UC_ROOT_DIR."inc/class_templates.php";
	include UC_ROOT_DIR."inc/class_database.php";
	include UC_ROOT_DIR."inc/functions.php";

	//And define classes
	$openid = new LightOpenID($settings['steam']['hostname']);
	$openid->identity = $settings['steam']['identity'];
	$parsedown = new ParsedownQuotes();
	$parsedown->setbreaksEnabled(true);

	//Pretty nice error page.
	use Whoops\Handler\PrettyPageHandler;
	use Whoops\Run;

	$run     = new Run();
	$handler = new PrettyPageHandler();

	$run->pushHandler($handler);

	$run->pushHandler(function ($exception, $inspector, $run) {
	    $inspector->getFrames()->map(function ($frame) {
	        if ($function = $frame->getFunction()) {
	            $frame->addComment("This frame is within function '$function'", 'cpt-obvious');
	        }
	        return $frame;
	    });

	});

	$run->register();

	//And now some language. By default we have english version, if user specifies other languages, load it.
	include UC_ROOT_DIR."inc/language.php";

	if(function_exists('date_default_timezone_set') && !ini_get('date.timezone')) {
		date_default_timezone_set('GMT');
	}

	$db = new MyDatabaseConnector;
	$db->connect($settings['sql']);
	$db->select_db($settings['sql']['dbname']);

	//Now some copyright
	if(date("Y") == "2016") {
		$current_year = date("Y");
	} else {
		$current_year = "2016-".date("Y");
	}
	
	$timenow = time();
	
	//User authenication goes here
	$me = array();

	if(@$_COOKIE['session_id'] != NULL && @$_COOKIE['session_token'] !== NULL) {
		$session_id		 = $db->escape_string($_COOKIE['session_id']);
		$session_token	 = $db->escape_string($_COOKIE['session_token']);

		if($_SERVER['DOCUMENT_URI'] == "/") {
			$_SERVER['REQUEST_URI'] = str_replace("/?", "/index.php?", $_SERVER['REQUEST_URI']);
		}

		$query = $db->query("SELECT * FROM `sessions` WHERE `session_id` = '{$session_id}' AND `session_token` = '{$session_token}'");
		if($db->num_rows($query) == '0') {
			//Hacking attempt... i fuckin hate it... let's delete those cookies.
			deletecookie("session_id");
			deletecookie("session_token");
			inline_message($settings['core']['siteurl'], $l['hackattempt']);
		} else {
			//fetch session details
			$row_fields = $db->fetch_array($query);

			//We know, that some users would try to make custom useragent just to SQLInject.
			$ua_escaped = $db->escape_string($_SERVER['HTTP_USER_AGENT']);

			//We also know, that some users would try to SQL Inject every location
			$location_escaped = $db->escape_string($_SERVER['REQUEST_URI']);

			//UPDATEQUERY (time_lastactivity, useragent)
			$db->query("UPDATE `sessions` SET `time_lastactivity` = '{$timenow}', `useragent` = '{$ua_escaped}', `location` = '{$location_escaped}'  WHERE `session_id` = '{$session_id}' AND `session_token` = '{$session_token}'");
			$db->query("UPDATE `users` SET `lastonline` = '{$timenow}' WHERE `id` = '{$row_fields['userid']}'");

			$query = $db->query("SELECT * FROM `users` WHERE `id` = '{$row_fields['userid']}'");

			//And push those details inside an array
			$me['userdetails'] = $db->fetch_array($query);
			$me['userdetails']['settings'] = json_decode(json_newline($me['userdetails']['settings']), true);
			$me['session'] = $row_fields;
		}
	}

	//all defined constants inside array.
	foreach(get_defined_constants(true)['user'] as $key => $val) {
		$defined[$key] = $val;
	}

	//Trailing slash...
	if(substr($settings['core']['siteurl'], -1) == "/") {
		$settings['core']['siteurl'] = substr($settings['core']['siteurl'], 0, -1);
	}

	//Inline success/error messages.
	if(isset($_SESSION['inline'])) {
		eval('$inline_message = "'.render_template('inline_message').'";');
		unset($_SESSION['inline']);
	} else {
		$inline_message = NULL;
	}

	//Navigation
	add_breadcrumb('<i class="fa fa-home" aria-hidden="true"></i>', "/");

	//Let's tweak with menu active
	$menu_active = str_replace(".php", "", end(explode("/", $_SERVER['SCRIPT_NAME'])));
	$menu_css[$menu_active] = "active";

	//Formatting
	$username = nick_format($me['userdetails'], false);

	//Steam
	$SteamCommunity['PersonaState'] = array(
		'0' 		=> $l['core_offline'],
		'1' 		=> $l['core_online'],
		'2' 		=> $l['core_busy'],
		'3' 		=> $l['core_away'],
		'4' 		=> $l['core_snooze'],
		'5' 		=> $l['core_lookingtotrade'],
		'6' 		=> $l['core_lookingtoplay'],
	);

	$SteamCommunity['PersonaStateFlag'] = array(
		'1'			=> $l['steam_hasrichpresence'],
		'2'			=> $l['steam_injoinablegame'],
		'256'		=> $l['steam_usingweb'],
		'512'		=> $l['steam_usingmobile'],
		'1024'		=> $l['steam_usingbigpicture'],
		'1536'		=> $l['steam_usingbigpicture']
	);

	//Token
	$csrf_code = generate_csrf();

	//Does user is logged in?
	if($me['userdetails']['id']) {
		eval('$welcomeblock = "'.render_template('header_welcomeblock_loggedin').'";');
	} else {
		$l['nav_login'] = LANG::sprintf($l['nav_login'], "Steam");
		eval('$welcomeblock = "'.render_template('header_welcomeblock_notloggedin').'";');
	}

	eval('$headerinclude = "'.render_template('headerinclude').'";');
	eval('$header = "'.render_template('header').'";');
	eval('$footer = "'.render_template('footer').'";');

	//Check if board is closed
	if(strlen($settings['core']['closedinfo']) != '0' AND !in_array($_SERVER['PHP_SELF'], array('/login.php', '/logout.php')) AND $me['userdetails']['id'] != '1') {
		add_breadcrumb($l['nav_closed']);
		eval('$mainpage = "'.render_template('closed').'";');
		output_page($mainpage);
		exit;
	}
