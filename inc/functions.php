<?php

	//such line, much code, very wow.
	(!defined("UC_CORE")) ? die("You shall not pass!") : "";

	function random_str($length=8, $complex=false) {
		$set = array_merge(range(0, 9), range('A', 'Z'), range('a', 'z'));
		$str = array();

		if($complex == true) {
			$str[] = $set[rand(0, 9)];
			$str[] = $set[rand(10, 35)];
			$str[] = $set[rand(36, 61)];

			$length -= 3;
		}

		for($i = 0; $i < $length; ++$i) {
			$str[] = $set[rand(0, 61)];
		}
		
		shuffle($str);
		return implode($str);
	}

	function hexcolor($length = 6) {
		$set = array_merge(range(0, 9), range('A', 'F'));
		$str = array();
		for($i = 0; $i < $length; ++$i) {
			$str[] = $set[rand(0, 15)];
		}
		
		shuffle($str);
		return implode($str);
	}

	function render_template($templatename) {
		global $templates;

		return $templates->get($templatename);
	}

	function escape($message) {
		$message = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $message);
		$message = str_replace("<", "&lt;", $message);
		$message = str_replace(">", "&gt;", $message);
		$message = str_replace("\"", "&quot;", $message);
		return $message;
	}

	function inline_message($url, $message) {
		global $_SESSION;

		$_SESSION['inline'] = array(
			'message' => escape($message)
		);

		header("Location: ".$url);
		die();
	}

	function makecookie($name, $value="", $expires="", $httponly=false) {
		global $settings;

		if(!$settings['cookie']['path']) {
			$settings['cookie']['path'] = "/";
		}
		
		if($expires == -1) {
			$expires = 0;
		} elseif($expires == "" || $expires == null) {
			$expires = (int)'2147483647'; // Make the cookie expire in a years time
		} else {
			$expires = time() + (int)$expires;
		}

		$settings['cookie']['path'] = str_replace(array("\n","\r"), "", $settings['cookie']['path']);
		$settings['cookie']['domain'] = str_replace(array("\n","\r"), "", $settings['cookie']['domain']);

		$cookie = "Set-Cookie: {$name}=".urlencode($value);

		if($expires > 0) {
			$cookie .= "; expires=".@gmdate('D, d-M-Y H:i:s \\G\\M\\T', $expires);
		}
		
		if(!empty($settings['cookie']['path'])) {
			$cookie .= "; path={$settings['cookie']['path']}";
		}

		if(!empty($settings['cookie']['domain'])) {
			$cookie .= "; domain={$settings['cookie']['domain']}";
		}
		
		if($httponly == true) {
			$cookie .= "; HttpOnly";
		}

		header($cookie, false);
	}

	function deletecookie($name) {
		$expires = -3600;
		makecookie($name, "", $expires);
	}

	function add_breadcrumb($name, $url="") {
		global $nav, $nav_count;

		$nav_count = count($nav);
		$nav[$nav_count]['name'] = $name;
		$nav[$nav_count]['url'] = $url;
	}

	function output_page($content) {
		global $nav, $db;

		//Let's draw navigation bar
		$nav_count = count($nav)-1; //Is the last one

		foreach($nav as $key => $navigator) {
			if($key == $nav_count) {
				eval("\$breadcrumb_row .= \"".render_template("global_breadcrumb_row_last")."\";");
			} else {
				eval("\$breadcrumb_row .= \"".render_template("global_breadcrumb_row")."\";");
			}
		}

		eval("\$breadcrumb = \"".render_template("global_breadcrumb")."\";");

		$content = str_replace("<navbar>", $breadcrumb, $content);

		if(isset($_SERVER['HTTP_ACCEPT_ENCODING']))	$httpaccept_encoding = $_SERVER['HTTP_ACCEPT_ENCODING'];
		if(strpos(" ".$httpaccept_encoding, "x-gzip")) $encoding = "x-gzip";
		if(strpos(" ".$httpaccept_encoding, "gzip")) $encoding = "gzip";

		if($_GET['logging'] == "1") {
			echo "<pre>";
			print_r($db->get_list_of_queries());
			echo "<pre>";
		} else {
			//header("Content-Encoding: $encoding");
			//echo gzencode($content, '9');
			if($_GET['_pjax']) {
				$turbo = new Turbo\Turbo;
				echo $turbo->extract($content);
			} else {
				echo $content;
			}
		}
	}

	function nick_format($array, $clickable = true) {
		global $settings, $l;
		if(!is_array($array)) return;

		$uid 		= $array['id'];
		$username 	= $array['username'];
		$color 		= $array['color'];

		if($color == '000000') {
			$bolded = "font-weight: bold;";	
		} else {
			$bolded = "";	
		}

		if($uid == NULL || $username == NULL) {
			$format = "<span style='color: black; {$bolded}'>".$l['user_guest']."</span></a>";
		} else {
			if($clickable == true) {
				$format = "<a href='".$settings['core']['siteurl']."/profile/".$uid."-".slugify($username)."'><span style='color: #".$color."; {$bolded}'>".$username."</span></a>";
			} else {
				$format = "<span style='color: #".$color."; {$bolded}'>".$username."</span>";
			}
		}

		return $format;
	}

	function time_elapsed_string($stamp, $full = false) {
		global $l, $timenow;

		$stamp = (int)str_replace("@", "", $stamp);

		if($timenow>$stamp) {
			$diff = (int)$timenow-$stamp;
			$over = 'ago';
		} else {
			$diff = (int)$stamp-$timenow;
			$over = 'left';
		}

		$formatter['year']		= 31104000;
		$formatter['month']		= 2592000;
		$formatter['day']		= 86400;
		$formatter['hour']		= 3600;
		$formatter['minute']	= 60;
		$formatter['second']	= 1;

		foreach($formatter as $date => $overstamp) {
			$calc[$date] = ($reminder != NULL) ? floor($diff/$overstamp) % $formatter[$reminder] : floor($diff/$overstamp);
			$sp = ($calc[$date] == "1") ? "single" : "plural";

			if($calc[$date] == 0) {
				if($date != 'second') {
					unset($calc[$date]);
				}
			} else {
				$tostring[$date] = $calc[$date]." ".$l['rel_'.$date.'_'.$sp];
			}
			$reminder = $date;
		}

		if(count($calc) == '1' && $reminder = 'seconds') {
			if($calc['second'] == '0') {
				$tostring[$date] = $l['rel_justnow'];
			} else {
				$tostring[$date] = $l['rel_less_than'];
			}
		} else {
			//$tostring[$date] = $calc[$date]." ".$l['rel_'.$date.'_'.$sp]." ".$l['rel_ago'];
			$tostring[$date] = LANG::sprintf($l['rel_format'], $calc[$date]." ".$l['rel_'.$date.'_'.$sp], $l['rel_'.$over]);
		}

		if($full == false) {
			$display = LANG::sprintf($l['rel_format'], array_shift(array_slice($tostring, 0, 1)), $l['rel_'.$over]);
		} else {
			$display = implode(", ", $tostring);
		}

		return $display;
	}

	function generate_csrf() {
		global $me, $_SERVER;

		return $me['userdetails']['id'] ? sha1($me['userdetails']['id'].$me['userdetails']['username'].$me['session']['useragent']) : md5($_SERVER['HTTP_USER_AGENT']);
	}

	function validate_csrf($code) {
		return (generate_csrf() != $code) ? false : true;
	}

	function pretty_location($uri) {
		global $l;

		return NULL; //"Watching MainPage (".$uri.")";
	}

	function report_irc($postmeonirc, $channel = "#UC") {
		$socket = fsockopen("irc.nuclearfallout.net", "6667");
		fputs($socket,"CAP LS \r\n");
		fputs($socket,"NICK UltimateClan \r\n");
		fputs($socket,"USER UC 0 * :... \r\n");
		$x = 1;
		while($x <= 25) {
			$sock_data = fgets($socket,1024);
			$exp = explode(":", $sock_data);
			$exp2 = explode(" ", $sock_data);
			$message = $exp['1'];
			$who = $exp['0'];
			$ver = $exp2['3'];
			if ($who == 'PING ') { 
				fputs($socket,"PONG $message \r\n"); 
			}
			if($x == "23") {
				fputs($socket,"PRIVMSG UCVerse :q1w2e3 PRIVMSG ".$channel." :4[-WEB-] ".$postmeonirc." \r\n"); 
			}
			if($x == "28") {
				fclose($socket);
			}
			$x++;
		}
	}

	function slugify($text) {
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}

	function create_texteditor($fieldname = 'message', $content = "") {
		if($content == NULL) {
			$content = $_POST[$fieldname];
		}
		eval("\$editor = \"".render_template("global_editor")."\";");
		return $editor;
	}

	function textcut($text, $limit = 250) {
		global $timenow;
		$text = strip_tags($text);

		$textcut = explode(md5($timenow), wordwrap($text, $limit, md5($timenow), false))['0'];

		if($text != $textcut) {
			$textcut = $textcut." ...";
		}

		return $textcut;
	}

	function rank($exp) {
		$entrylvl = "200";
		$increases = "17";

		$i = '0';

		while ($i <= 36) {
			$exp_x_lvl[$i] = (int)(($entrylvl+$increases)*$i);
			$i++;
		}

		foreach($exp_x_lvl as $key => $value) {
			if($exp < $value && !isset($return)) {
				$return = array('exp' => $exp, 'rank' => $key, 'next_exp' => $value, 'missing_exp' => $value-$exp);
			}
		}

		if(!isset($return)) {
			$return = array('exp' => $exp, 'rank' => '40', 'next_exp' => '0', 'over_exp' => $exp-$value);
		}

		return $return;
	}

	function points_calculate($winner_current_points, $loser_current_points, $max_possible_points_to_gain = 80) {
		$their_dif_for_winner = $loser_current_points - $winner_current_points;
		$their_dif_for_loser = $winner_current_points - $loser_current_points;

		$expectedW = 1 / (1 + pow(10, ($their_dif_for_winner / 1000)));
		$expectedL = 1 / (1 + pow(10, ($their_dif_for_loser / 1000)));

		$gained = $max_possible_points_to_gain * (1 - $expectedW);
		$lost = $max_possible_points_to_gain * (0 - $expectedL);

		return array(
			'new_winner_points' => ($winner_current_points+(floor($gained) == 0 ? 1 : floor($gained))),
			'new_loser_points' => (($loser_current_points+(ceil($lost) == 0 ? -1 : ceil($lost))) >= 0) ? ($loser_current_points+(ceil($lost) == 0 ? -1 : ceil($lost))) : '0'
		);
	}

	function inline_alerts($alert_message, $alert_type = 'alert') {
		global $l;

		$warning_msg = $l['core_'.$alert_type.'_warning'];

		eval('$return = "'.render_template('global_alerts').'";');
		return $return;
	}

	function getUserSetting($setting) {
		global $userDefaults, $userdetails, $me;

		if(isset($userdetails) && $userdetails['id'] != $me['userdetails']['id']) {
			$usersettings = json_decode($userdetails['settings'], true);
		} else {
			$usersettings = $me['userdetails']['settings'];
		}

		return (isset($usersettings[$setting])) ? $usersettings[$setting] : $userDefaults[$setting];
	}

	function log_action($actiontype, $comment = "", $userid = "loggedin") {
		global $db, $me;

		if($userid == "loggedin") {
			$userid = $me['userdetails']['id'];
		}

		$actiontype = $db->escape_string($actiontype);
		$comment = $db->escape_string($comment);

		$db->query("INSERT INTO `actions` (`id`, `uid`, `type`, `comment`, `ip`) VALUES (NULL, '{$userid}', '{$actiontype}', '{$comment}', '{$_SERVER["REMOTE_ADDR"]}')");
	}

	function inline_error($array) {
		global $l;

		$errorhandler = "<div class='error'>";
		$errorhandler .= "<p>{$l['core_errorlist']}</p>";
		$errorhandler .= "<ul>";
		foreach ($array as $value) {
			$errorhandler .= "<li>".$value."</li>";
		}
		$errorhandler .= "</ul>";					
		$errorhandler .= "</div>";

		return $errorhandler;
	}

	function validate_nick($nick) {
		return preg_match('/^[\-a-zA-Z\[\]_|`^][\-a-zA-Z0-9\[\]_|`^]{0,16}$/', $nick);
	}

	function validate_date($date) {
		if(!preg_match("/((((1[26]|2[048])00)|[12]\d([2468][048]|[13579][26]|0[48]))-((((0[13578]|1[02])-(0[1-9]|[12]\d|3[01]))|((0[469]|11)-(0[1-9]|[12]\d|30)))|(02-(0[1-9]|[12]\d))))|((([12]\d([02468][1235679]|[13579][01345789]))|((1[1345789]|2[1235679])00))-((((0[13578]|1[02])-(0[1-9]|[12]\d|3[01]))|((0[469]|11)-(0[1-9]|[12]\d|30)))|(02-(0[1-9]|1\d|2[0-8]))))/", $date)) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	function json_newline($text) {
	    $text = str_replace("\r\n", "\n", $text);
	    $text = str_replace("\r", "\n", $text);
	    $text = str_replace("\n", "\\n", $text);
	    return $text;
	}

	function json_fix($text) {
		return str_replace("\\\\", "\\", $text);
	}

	function SecureToken($text, $expire = '20') {
		$array['token']['current'] = substr(strtoupper(sha1(hash_hmac('md5', pack('N*', 0) . pack('N*', floor(time()/$expire)), $text, true))), 0, 20);
		$array['token']['next'] = substr(strtoupper(sha1(hash_hmac('md5', pack('N*', 0) . pack('N*', floor((time()+$expire)/$expire)), $text, true))), 0, 20);
		$array['expire'] = $expire-(time()%$expire);
		return json_encode($array);
	}