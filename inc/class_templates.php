<?php

	class templates {
		function get($title, $eslashes=1, $htmlcomments=1) {
			$template = @file_get_contents("./templates/".$title.".tpl");

			$output = "<!-- START: {$title} -->";
			$output .= str_replace("\\'", "'", addslashes($template));
			$output .= "<!-- END: {$title} -->";
			return $output;
		}
	}

	eval('
		class phptpl_templates extends templates {
			function phptpl_templates(&$oldtpl) {
				$this->parsed_cache = array();
			}

			function get($title, $eslashes=1, $htmlcomments=1) {
				if($eslashes) {
					$r = parent::get($title, $eslashes, $htmlcomments);
					if(!isset($this->parsed_cache[$title]) || $this->parsed_cache[$title][0] != $r)	{
						$this->parsed_cache[$title] = array($r, $r);
						phptpl_parsetpl($this->parsed_cache[$title][1]);
					}
					return $this->parsed_cache[$title][1];
				}
				else
					return parent::get($title, $eslashes, $htmlcomments);
			}
		}
	');

	function phptpl_parsetpl(&$ourtpl) {
		$GLOBALS['__phptpl_if'] = array();
		$ourtpl = preg_replace(array(
			'#\<((?:else)?if\s+(.*?)\s+then|else\s*/?|/if)\>#sie', // note that this relies on preg_replace working in a forward order
			'#\<func (htmlspecialchars|htmlspecialchars_uni|intval|floatval|urlencode|rawurlencode|addslashes|stripslashes|trim|crc32|ltrim|rtrim|chop|md5|nl2br|sha1|strrev|strtoupper|strtolower|my_strtoupper|my_strtolower|alt_trow|get_friendly_size|filesize|strlen|my_strlen|my_wordwrap|random_str|unicode_chr|bin2hex|str_rot13|str_shuffle|strip_tags|ucfirst|ucwords|basename|dirname|unhtmlentities)\>#i',
			'#\</func\>#i',
			'#\<template\s+([a-z0-9_ \-+!(),.]+)(\s*/)?\>#ie',
			'#\<\?=(.*?)\?\>#se',
			'#\<setvar\s+([a-z0-9_\-+!(),.]+)\>(.*?)\</setvar\>#ie',
			'#\<\?(?:php|\s).+?(\?\>)#se', // '#\<\?.*?(\?\>|$)#se',
		), array(
			'phptpl_if(\'$1\', phptpl_unescape_string(\'$2\'))',
			'".$1("',
			'")."',
			'$GLOBALS[\'templates\']->get(\'$1\')',
			'\'".strval(\'.phptpl_unescape_string(\'$1\').\')."\'',
			'\'".(($GLOBALS["tplvars"]["$1"] = \'.phptpl_unescape_string(\'$2\').\')?"":"")."\'',
			'phptpl_evalphp(phptpl_unescape_string(\'$0\'), \'$1\')',
		), $ourtpl);
	}

	function phptpl_unescape_string($str)
	{
		return strtr($str, array('\\\\"' => '"', '\\\\' => '\\'));
	}

	function phptpl_if($s, $e) {
		if($s[0] == '/') {
			// end if tag
			$last = array_pop($GLOBALS['__phptpl_if']);
			$suf = str_repeat(')', (int)substr($last, 1));
			if($last[0] == 'i')
				$suf = ':""'.$suf;
			return '"'.$suf.')."';
		} else {
			$s = strtolower(substr($s, 0, strpos($s, ' ')));
			if($s == 'if') {
				$GLOBALS['__phptpl_if'][] = 'i0';
				return '".(('.$e.')?"';
			} elseif($s == 'elseif') {
				$last = array_pop($GLOBALS['__phptpl_if']);
				$last = 'i'.((int)substr($last, 1) + 1);
				$GLOBALS['__phptpl_if'][] = $last;
				return '":(('.$e.')?"';
			} else {
				$last = array_pop($GLOBALS['__phptpl_if']);
				$last[0] = 'e';
				$GLOBALS['__phptpl_if'][] = $last;
				return '":"';
			}
		}
	}

	function phptpl_evalphp($str, $end) {
		return '".eval(\'ob_start(); ?>'
			.strtr($str, array('\'' => '\\\'', '\\' => '\\\\'))
			.($end?'':'?>').'<?php return ob_get_clean();\')."';
	}

	// compatibility functions with Template Conditionals plugin
	function phptpl_eval_expr($__s) {
		return eval('return ('.$__s.');');
	}

	function phptpl_eval_text($__s) {
		// simulate $templates->get()
		$__s = strtr($__s, array('\\' => '\\\\', '"' => '\\"', "\0" => ''));
		phptpl_parsetpl($__s);
		return eval('return "'.$__s.'";');
	}

	$templates = new templates($templates);