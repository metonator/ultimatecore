<?php

	$l['helloworld']				 = "Hello World";
	$l['hackattempt']				 = "Your session has expired. Logged out.";

	$l['user_logincorrect']			 = "Hello again, {1}!";	// {1} = USERNAME
	$l['user_logindeclined']		 = "Your login attempt has been refused.";
	$l['user_loginincorrect']		 = "Failed to log in.";
	$l['user_alreadyloggedin']		 = "You are already logged in, {1}."; // {1} = USERNAME
	$l['user_notloggedin']			 = "You are not logged in.";
	$l['user_loggedout']			 = "See you later, {1}!"; // {1} = USERNAME
	$l['user_unkownuser']			 = "User doesn't exist!";
	$l['user_guest']				 = "Guest";

	$l['nav_profile']				 = "Profile of {1}"; // {1} = USERNAME
	$l['nav_altprofile']			 = "Profile";
	$l['nav_homepage']				 = "Main Page";
	$l['nav_forum']					 = "Forum";
	$l['nav_league']				 = "League";
	$l['nav_challenges']			 = "Challenges";
	$l['nav_shoutbox']				 = "Chat";
	$l['nav_login']					 = "Log in with {1}"; // {1} = LOGIN PROVIDER
	$l['nav_logout']				 = "Log out";
	$l['nav_settings']				 = "Settings";
	$l['nav_closed']				 = "Site Closed";
	$l['nav_inbox']					 = "Inbox";
/**/$l['nav_pages']					 = "Pages";
/**/$l['nav_schemes']				 = "Schemes";

	$l['logout_inuse']				 = "In use right now";
	$l['logout_confirm_title']		 = "Do you want to log out?";
	$l['logout_sessionlist']		 = "This is a list of devices that have logged into your account:";
	$l['logout_destroysessions']	 = "Destroy all existing sessions";
	$l['logout_confirm']			 = "Yes, i want to log out";

	$l['core_csrferror']			 = "CSRF Error. Please try again";
	$l['core_loginrequired']		 = "Please log in before trying this action";
	$l['core_errorlist']			 = "There was some error:";
	$l['core_alert_warning']		 = "Alert:";
	$l['core_unknown']				 = "Unknown";
	$l['core_offline']				 = "Offline";
	$l['core_online']				 = "Online";
	$l['core_busy']					 = "Busy";
	$l['core_away']					 = "Away";
	$l['core_snooze']				 = "Snooze";
	$l['core_lookingtotrade']		 = "Looking for Trade";
	$l['core_lookingtoplay']		 = "Looking for Game";
/**/$l['core_yes']					 = "Yes";
/**/$l['core_no']					 = "No";
/**/$l['core_notallowed']			 = "Sorry, but you can not take this operation";

	$l['steam_hasrichpresence']		 = "Has Rich Presence";
	$l['steam_injoinablegame']		 = "Joinable Game";
	$l['steam_usingweb']			 = "Web";
	$l['steam_usingmobile']			 = "Mobile";
	$l['steam_usingbigpicture']		 = "Big Picture";
	$l['steam_status']				 = "{1} on {2}";
	$l['steam_playing']				 = "Playing <a href='https://store.steampowered.com/app/{2}'>{1}</a>";

	$l['forum_noexist']				 = "Forum does not exist";
	$l['forum_threadnoexist']		 = "Discussion does not exist";
	$l['forum_newdiscussion']		 = "Start a discussion";
	$l['forum_nothreads']			 = "Sorry, but there's no discussion for {1} players"; // {1} = LANGUAGE
	$l['forum_description']			 = "A dedicated forum for {1} players"; // {1} = LANGUAGE
	$l['forum_langbreadcrumb']		 = "{1} forum"; // {1} = LANGUAGE
	$l['forum_thread_title']		 = "Discussion #{1}: {2}"; // {1} = NUMBER | {2} = THREADNAME
	$l['forum_author_wrote']		 = "{1} wrote {2}:"; // {1} = USERNAME | {2} = TIMEAGO
	$l['forum_author_iswriting']	 = "{1} is writing:"; // {1} = USERNAME
	$l['forum_reply_notloggedin']	 = "You must be logged in to reply to this discussion";
	$l['forum_reply_button']		 = "Reply to this discussion";
	$l['forum_discussionname']		 = "Discussion name";
	$l['forum_usercreated']			 = "<i class='fa fa-paper-plane' aria-hidden='true'></i> {1} created this topic {2}"; // {1} = USERNAME | {2} = TIMEAGO
	$l['forum_userreplied']			 = "<i class='fa fa-reply' aria-hidden='true'></i> {1} replied to this topic {2}"; // {1} = USERNAME | {2} = TIMEAGO
	$l['forum_shorttitle']			 = "Topic name must be between 3 and 120 characters.";
	$l['forum_writesomething']		 = "Please write something";
	$l['forum_adminonly']			 = "Sorry, but only administrators can post in this forum.";
/**/$l['forum_sub_usercreated']		 = "<i class='fa fa-paper-plane' aria-hidden='true'></i> {1} created a discussion {2}"; // {1} = USERNAME | {2} = TIMEAGO
/**/$l['forum_sub_userreplied']		 = "<i class='fa fa-reply' aria-hidden='true'></i> {1} replied to a discussion {2}"; // {1} = USERNAME | {2} = TIMEAGO
/**/$l['forum_sub_nothreads']		 = "<i class='fa fa-reply' aria-hidden='true'></i> Nobody yet made any thread here";
/**/$l['forum_edit_head']			 = "{1} edited this post {2}";
/**/$l['forum_reportreason']		 = "Why do you wanna report this post?";
/**/$l['forum_reportwrite']			 = "Please, type a reason for this post";
/**/$l['forum_deletepost']			 = "Are you sure you wanna delete this post?";
/**/$l['forum_reported']			 = "You have successfully reported that post";
/**/$l['forum_subforums']			 = "Subforums";
/**/$l['forum_discussions']			 = "Discussions";
/**/$l['forum_navigation']			 = "Navigation";

	$l['mainpage_stats_title']		 = "Statistics:";
	$l['mainpage_stats_posts']		 = "{1} posts"; // {1} = Posts
	$l['mainpage_stats_discussions'] = "{1} discussions"; // {1} = Discussions
	$l['mainpage_stats_users']		 = "{1} users"; // {1} = Usercount
	$l['mainpage_stats_joined']		 = "{1} joined"; // {1} = Usercount

	$l['language_all']				 = "International";
	$l['language_bug']				 = "Bug Reports";
	$l['language_ann']				 = "Announcements";
	$l['language_tr']				 = "Turkish";
	$l['language_pl']				 = "Polish";

	$l['ranked_1']	 				 = "Beginner 1";
	$l['ranked_2'] 					 = "Beginner 2";
	$l['ranked_3'] 					 = "Beginner 3";
	$l['ranked_4'] 					 = "Beginner Elite";
	$l['ranked_5'] 					 = "Rookie 1";
	$l['ranked_6'] 					 = "Rookie 2";
	$l['ranked_7'] 					 = "Rookie 3";
	$l['ranked_8'] 					 = "Rookie Elite";
	$l['ranked_9'] 					 = "Average 1";
	$l['ranked_10'] 				 = "Average 2";
	$l['ranked_11'] 				 = "Average 3";
	$l['ranked_12'] 				 = "Average Elite";
	$l['ranked_13'] 				 = "Competent 1";
	$l['ranked_14'] 				 = "Competent 2";
	$l['ranked_15'] 				 = "Competent 3";
	$l['ranked_16'] 				 = "Competent Elite";
	$l['ranked_17']				 	 = "Challenger 1";
	$l['ranked_18'] 				 = "Challenger 2";
	$l['ranked_19'] 				 = "Challenger 3";
	$l['ranked_20'] 				 = "Challenger Elite";
	$l['ranked_21'] 				 = "Pro 1";
	$l['ranked_22'] 				 = "Pro 2";
	$l['ranked_23'] 				 = "Pro 3";
	$l['ranked_24'] 				 = "Pro Elite";
	$l['ranked_25'] 				 = "Expert 1";
	$l['ranked_26'] 				 = "Expert 2";
	$l['ranked_27'] 				 = "Expert 3";
	$l['ranked_28'] 				 = "Expert Elite";
	$l['ranked_29'] 				 = "Master 1";
	$l['ranked_30'] 				 = "Master 2";
	$l['ranked_31'] 				 = "Master 3";
	$l['ranked_32'] 				 = "Master Elite";
	$l['ranked_33'] 				 = "Ultimate 1";
	$l['ranked_34'] 				 = "Ultimate 2";
	$l['ranked_35'] 				 = "Ultimate 3";
	$l['ranked_36'] 				 = "Ultimate Elite";

	$l['profiles_nofriends']	 	 = "There are no friends registered on that site, or profile is not set to public on Steam";
	$l['profiles_friendlist']		 = "Friends:";
	$l['profiles_notexists']		 = "This profile doesn't exist";
	$l['profiles_vacBanned']		 = "This player has been VAC Banned {1} times {2} days ago."; // {1} = NUMBER | {2} = NUMBER
	$l['profiles_gameBanned']		 = "This player has been Game Banned {1} times {2} days ago."; // {1} = NUMBER | {2} = NUMBER
	$l['profiles_communityBanned']	 = "This player has been Community Banned.";
	$l['profiles_customquote']		 = "My quote:";
	$l['profiles_about']			 = "About";
	$l['profiles_additionalinfo']	 = "Additional information:";
	$l['profiles_posts']			 = "Posts:";
	$l['profiles_threads']			 = "Discussions:";
	$l['profiles_country']			 = "Country:";
	$l['profiles_lastonline']		 = "Last online:";
	$l['profiles_website']			 = "Website Online:";
	$l['profiles_wormnet']			 = "WormNET Online:";
	$l['profiles_snooper']			 = "Snooper Online:";
	$l['profiles_steam']			 = "Steam Online:";
	$l['profiles_addasfriend']		 = "Add as friend";
	$l['profiles_sendmessage']		 = "Send a Message";

	$l['settings_general']			 = "General Settings";
	$l['settings_additional']		 = "Additional Settings";
	$l['settings_nickname']			 = "Nickname:";
	$l['settings_avatar']			 = "Avatar:";
	$l['settings_nickname']			 = "Nickname:";
	$l['settings_nick_usesteam']	 = "Use default nickname from Steam";
	$l['settings_avatar_usesteam']	 = "Use default avatar from Steam";
	$l['settings_wormnet'] 			 = "WormNET Nickname:";
	$l['settings_snooper'] 			 = "Snooper Nickname:";
	$l['settings_city'] 			 = "City:";
	$l['settings_country'] 			 = "Country:";
	$l['settings_birthdate'] 		 = "Birthdate:";
	$l['settings_customquote']		 = "Custom Quote:";
	$l['settings_updated']			 = "Profile has been updated!";
	$l['settings_nicktooshort']		 = "Nickname is too long. Maximum length is 40.";
	$l['settings_nonick']			 = "Please type your nickname or mark a checkbox";
	$l['settings_avatarupfailed']	 = "Please upload an avatar or mark a checkbox";
	$l['settings_avatarwrongmime']	 = "Wrong file extension. We only accept PNG, GIF, JPG and JPEG";
	$l['settings_avatarexceeded']	 = "Please upload a 200x200 avatar image";
	$l['settings_wrongwormnet']		 = "Bad WormNET Nickname. Use only strings from A to Z including numbers and -`_";
	$l['settings_wrongsnooper']		 = "Bad Snooper Nickname. Use only strings from A to Z including numbers and -`_";
	$l['settings_wrongcountry']		 = "This country doesn't exist";
	$l['settings_wrongcity']		 = "Surely this city doesn't exist";
	$l['settings_wrongbirthdate']	 = "Wrong date. Use YYYY-MM-DD format";
	$l['settings_quotetoolong']		 = "Quote is too long. Maximum length is 300";

	$l['groups_level0'] 			 = "Banned/Restricted People";
	$l['groups_level1'] 			 = "Normal Member";
	$l['groups_level2'] 			 = "Fairly Better Member";
	$l['groups_level3'] 			 = "Planned Recruit";
	$l['groups_level4'] 			 = "UC Member";
	$l['groups_level5'] 			 = "Fairly More Trustworthy UC Member";
	$l['groups_level6'] 			 = "SUCCESSful UC Member";
	$l['groups_level7'] 			 = "Website Owner";
	$l['groups_level8'] 			 = "Founder";

	$l['main_langbreadcrumb']		 = "{1}' Main Page";
	$l['main_noexist']				 = "This community doesn't exist";
	$l['main_nothreads']			 = "Sorry, but there's no discussions yet";
	$l['main_noannouncement']		 = "There's no announcements yet";
	$l['main_chat_information']		 = "We use discord as our chat system. Come and talk with us :D";
	$l['main_chat_join']			 = "Open Discord.GG and enter UC chatroom";
	$l['main_chat_join_irc']		 = "Or open IRC Alternative";
	$l['main_welcomebox']			 = "Welcome to the official website of Ultimate Clan! UC is a prestigious clan founded on July 2nd 2012; it has been active since then. Enjoy your stay!";
	$l['main_announcement'] 		 = "Announcements";
	$l['main_latestdiscossions']	 = "Latest discussions";

/**/$l['pages_a-test-page']			 = "UCVerse Documentation from GitHub";

/**/$l['rel_justnow'] 				 = "Just now";
/**/$l['rel_ago']					 = "ago";
/**/$l['rel_left']					 = "left";
/**/$l['rel_less_than']				 = "Less than one minute";
/**/$l['rel_second_single']			 = "second";
/**/$l['rel_second_plural']			 = "seconds";
/**/$l['rel_minute_single']			 = "minute";
/**/$l['rel_minute_plural']			 = "minutes";
/**/$l['rel_day_single']			 = "day";
/**/$l['rel_day_plural']			 = "days";
/**/$l['rel_hour_single']			 = "hour";
/**/$l['rel_hour_plural']			 = "hours";
/**/$l['rel_month_single']		 	 = "month";
/**/$l['rel_month_plural']		 	 = "months";
/**/$l['rel_year_single']			 = "year";
/**/$l['rel_year_plural']			 = "years";
/**/$l['rel_format']				 = "{1} {2}"; // {1} is formatted date, {2} is AGO or LEFT